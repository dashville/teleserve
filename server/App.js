import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import logger from 'morgan';
import passport from 'passport';
import router from "./routes/route";
import config from './config/config'
import * as database from './config/dbConfig';
import AuthUtils from './utils/AuthUtils'
import SourceMapSupport from 'source-map-support';
import Socket from './Socket'
import fs from 'fs'
import cors from 'cors'

passport.use(AuthUtils.strategy());

const app = new express();
const port = (process.env.PORT || 9000)

database.connect()
app.set('superSecret', config.secret)
//bb.extend(app);
//configure app'
app.use(cors())
app.disable('x-powered-by')// vulnerable header for Attackers
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use((req, res, next) => {
  const origin = req.get('origin');

  // TODO Add origin validation
  res.header('Access-Control-Allow-Origin', origin);
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control, Pragma');

  // intercept OPTIONS method
  if (req.method === 'OPTIONS') {
    res.sendStatus(204);
  } else {
    next();
  }
});
app.use('/', router);

SourceMapSupport.install();
const key = fs.readFileSync(__dirname + '/key.pem', 'utf8')
const cert = fs.readFileSync(__dirname + '/cert.pem', 'utf8')
const sslOptions = {
    key,
    cert,
    requestCert: false,
    rejectUnauthorized: false,
    passphrase: config.passphrase
}

/*https.createServer(sslOptions,app).listen(port, function () {
 console.log(`Server running on port ${port}`)
 });*/
const server = app.listen(port, function () {
    console.log(`Server running on port ${port}`)
});

const socket = new Socket({server})
socket.openSocket()

module.exports = app