import WebSocket from 'ws'
import _ from 'lodash'
import DeviceManager from './controllers/DeviceManager'
const command = {
    handshake: 'handshake',
    updateLocation: 'update-location'
}
class Socket {
    constructor(params) {
        this.params = params
        this.openSocket = this._openSocket.bind(this)
        this.sendMessage = this._onSendMessage.bind(this)
    }
    _onSendMessage(data){
        this.websocket.send(JSON.stringify(data))
    }

    _openSocket() {
        const server = this.params.server
        const socket = new WebSocket.Server({server})
        socket.on("connection", (ws) => {
            this.websocket = ws
            console.log("Socket.openSocket.connection is open ********");
            if (ws.readyState === ws.OPEN) {
                ws.on('message', (msg) => {
                    const message = JSON.parse(msg)
                    const commandId = message['command-id']
                    switch (commandId) {
                        case command.handshake:
                            console.log('App.onmessage.handshake. ===>', message.data)
                            break;
                        case command.updateLocation:
                            const {coords,uniqueId} = message.data
                            if (coords) {
                                DeviceManager.updateLocation(this.sendMessage,uniqueId,coords)
                            }
                            break;
                    }
                })
            }
        });
         socket.on('open', ws => {
            console.log('WebSocket._openSocket.websocket connection open -----------------------')
        })
    }
}

module.exports = Socket