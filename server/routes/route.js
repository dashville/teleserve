import express from "express";
import multer from 'multer';

import _ from 'lodash'
import passport from 'passport'
import * as controller from "../controllers/controller"
import Authenticate from "../controllers/Authenticate"
import FileManager from "../controllers/FileManager"
import UserController from '../controllers/UserController'
import Admin from '../controllers/admin/Admin'
import DeviceManager from '../controllers/DeviceManager'
import SmsApi from '../api/SmsApi'

const router = new express.Router();
const upload = multer({dest: 'uploads/'})

router.route('/').get(controller.getUser)

router.route('/images/:id').get(FileManager.imageLookUp)


router.route('/mobile/v1/auth/opt/:phone').get(SmsApi.requestOTP)

router.route('/mobile/v1/ping').get(DeviceManager.ping)

router.route('/mobile/v1/auth/login').post(Authenticate.login)
router.route('/mobile/v1/auth/logout').post(Authenticate.logout)
router.route('/mobile/v1/auth/register').post(Authenticate.signup)
router.route('/mobile/v1/auth/reset').post(Authenticate.resetPassword)
router.route('/mobile/v1/auth/validate-token').post(Authenticate.vaidateToken)


router.route('/mobile/v1/activate-identity').post(UserController.activateIdentity)
router.route('/mobile/v1/faces/:id').get(UserController.retreiveFaces)

router.route('/mobile/v1/upload/image').post(upload.single('image'), FileManager.uploadImage)

router.route('*').all(Authenticate.authenticate)
router.route('/mobile/v1/devices').get(DeviceManager.retrieveDevices)
router.route('/mobile/v1/profile/update').patch(UserController.updateProfile)
router.route('/mobile/v1/profile').get(UserController.retrieveProfile)
router.route('/admin/users').get(Admin.retrieveUsers)
router.route('/admin/users/delete').post(Admin.deleteUsers)
router.route('/admin/user/:id').get(Admin.retrieveUserDetails)
router.route('/admin/user/:id/modify-roles').post(Admin.modifyRoles)
router.route('/admin/user/:id/update').post(Admin.updateUserDetails)
router.route('/admin/devices').get(Admin.retrieveDevices)
router.route('/admin/device/:id').get(Admin.retrieveDeviceDetails)
router.route('/admin/report/users').post(Admin.generateUsersReport)



//.post(controller.getUser)
//.put(controller.getUser)
/*router.route("/secret", passport.authenticate('jwt', {session: false})).get(function (req, res) {
 console.log('Success! You can not see this without a token', res)
 return res.json({message: "Success! You can not see this without a token"});
 })*/

export default router;