import fetch from 'node-fetch'
import _ from 'lodash'
import config from '../config/config'

class GeoApi {
    constructor() {
        this.reverseGeoCode = this._reverseGeoCode.bind(this)
    }

    _reverseGeoCode(coords) {
        // const {coords} = req.body
        const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${coords.latitude},${coords.longitude}&key=${config.googleKey}`
       // const onError = (err)=> {throw new Error(err)}
        return new Promise((resolve, reject) => {
            fetch(url).then((response) => {
                try {
                    return response.json()
                }catch(err){
                     console.log('GeoApi.reverseGeoCode.err', err)
                }
            }).then((responseData) => {
                const status = responseData.status
                if (status == "OK") {
                    resolve(responseData.results[0].formatted_address)
                } else {
                    reject(new Error('No address'))
                }
            }).catch((err) => {
                console.log('GeoApi.reverseGeoCode.error ----->', err)
                reject(err)
            })
        })
    }
}

module.exports = new GeoApi()