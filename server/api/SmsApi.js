import qs from 'qs'
import _ from 'lodash'
import config from '../config/config'
import fetch  from 'node-fetch'

class SmsApi {
    constructor() {
        this.requestOTP = this._requestOTP.bind(this)
    }

    _requestOTP(req, res) {
        const params = req.params
        const otp = this._generateOTP()
        const query = qs.stringify({
            apiKey: config.smsApiKey,
            to: params.phone,
            content: otp
        })
        const url = `https://platform.clickatell.com/messages/http/send?${query}`
        fetch(url).then((resp) => {
            return resp.json()
        }).then((respData) => {
            const messages = respData.messages
            if(_.size(messages)>0){
                return res.json({success: true, item: otp})
            }else{
                return res.json({success: false, message:`Couldn't get otp`})
            }
        }).catch((err) => {
            console.log('SmsApi._requestOTP.err ===>', err)
            return res.json({success: false, message: err})
        })
    }

    _generateOTP() {
        return  Math.floor(100000 + Math.random() * 900000)
    }
}
module.exports = new SmsApi()