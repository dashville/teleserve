import mongoose from 'mongoose'
import bluebird from 'bluebird'
import config from './config'

export const connect = ()=>{
    mongoose.Promise = bluebird
    mongoose.connect(config.database, {
      //  useMongoClient: true,
        promiseLibrary: bluebird
    }).then(()=>{
        console.log('**** server.config.dbconnect successful ****')
    }).catch((err)=>{
        console.log('**** server.config.dbconnect unsuccessful ****', err)
    })
}