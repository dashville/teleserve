import bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

class Encryption {
    constructor() {
        this.encryptpwd = this._encryptpwd.bind(this);
        this.comparepwd = this._comparepwd.bind(this)
    }

    _encryptpwd(password) {
        return new Promise((resolve, reject) => {
                bcrypt.hash(password, SALT_ROUNDS, (err, hash) => {
                    if (err) reject(err);
                    resolve(hash)
                })
        })
    }

    _comparepwd(password, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, hash, (err, isPwdMatching) => {
                if (err) reject(err)
                resolve(isPwdMatching);
            })
        })
    }
}
module.exports = new Encryption();