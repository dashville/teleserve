import jwt from 'jsonwebtoken';
import config from '../config/config'
import User from '../models/model.user'
import {  Strategy, ExtractJwt} from 'passport-jwt'

class AuthUtils {
    constructor() {
        this.strategy = this._setStrategy.bind(this)
        this.generateToken = this._generateToken.bind(this)
        this.validateToken = this._validateToken.bind(this)
    }

    _generateToken(payload) {
        return new Promise((resolve, reject) => {
            jwt.sign(payload, config.secret, (err, token) => {
                if (err) reject(err);
                resolve(token)
            })
        })
    }

    _validateToken(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) reject(err);
                resolve(decoded)
            })
        })
    }

    _setStrategy() {
        let jwtOptions = {}
        jwtOptions.jwtFromRequest = ExtractJwt.fromHeader('authorization');
        jwtOptions.secretOrKey = config.secret
        jwtOptions.issuer = 'teleserve';

       return new Strategy(jwtOptions, (jwt_payload, next) => {
            User.findOne({_id: jwt_payload.sub}, (err, user) => {
                if (err) {
                     return next(err, false)
                }
                if (user) {
                    return next(null, user);
                }
                return next(null, false);
            });
        });
    }
}

module.exports = new AuthUtils();