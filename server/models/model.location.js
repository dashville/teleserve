import mongoose from 'mongoose'

const locationSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    uniqueId: String,
    createdOn: {type: String,default: Date.now()},
    updatedOn: String,
    coords: {
        lat: String,
        lng: String
    },
    formattedAddress: String
})

module.exports = mongoose.model('Location', locationSchema)