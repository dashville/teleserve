import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String, lowercase: true,
        required: [true, "can't be blank"],
        unique: true,
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    name: {
        type: String,
        required: [true, "can't be blank"]
    },
    lastname: {
        type: String,
        required: [true, "can't be blank"]
    },
    password: {
        type: String,
        required: [true, "can't be blank"]
    },
    email: {
        type: String, lowercase: true,
        required: [true, "can't be blank"],
        unique: true,
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    phone: String,
    'phone-alt': String,
    createdOn: {type: String, default: Date.now()},
    updatedOn: String,
    admin: {type: Boolean, default: false},
    hasIdentity: {type: Boolean, default: false},
    faces: [],
    mainDevice: String,
    devices: [],
    roles: {type: Array, default: ['user']}
})
export default mongoose.model('User', userSchema)