import mongoose from 'mongoose'

const deviceSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    uniqueId: {
        type: String,
        required: [true, "can't be blank"],
        unique: true
    },
    deviceId: {
        type: String,
        required: [true, "can't be blank"],
        unique: true
    },
    locale: {
        type: String,
        required: [true, "can't be blank"]
    },
    country: {
        type: String,
        required: [true, "can't be blank"]
    },
    brand: {
        type: String,
        required: [true, "can't be blank"]
    },
    manufacturer: {
        type: String,
        required: [true, "can't be blank"]
    },
    timezone: String,
    userAgent: String,
    createdOn: {
        type: String,
        default: Date.now()
    },
    addedBy: {
        userId: {type: String, required: [true, "can't be blank"]},
        owner:  {type: String, required: [true, "can't be blank"]},
        'app-details': {
            bundleId: String,
            appVersion: String,
            buildNumber: String,
        }
    },
    simData: {
        phoneNumber: String,
        carrier: String,
        countryCode: String,
        simInfo: {}
    }
})

module.exports = mongoose.model('Device', deviceSchema);