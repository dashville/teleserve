import mongoose from 'mongoose'

const imageSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    data: Buffer,
    contentType: String,
    createdOn: {type: String, default: Date.now()},
    addedBy: String,
    //updateOn: String,
    //  small: String,
    //   medium: String,
    // large: String,
})

export default mongoose.model('Image', imageSchema)