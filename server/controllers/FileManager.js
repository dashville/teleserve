import Image from '../models/model.image'
import User from '../models/model.user'
import _ from 'lodash'
import fs from 'fs'
import PDFDocument from 'pdfkit'
import {Report} from 'fluentreports'
import ReportController from './ReportController'

class FileManager {
    constructor() {
        this.uploadImage = this._uploadImage.bind(this)
        this.imageLookUp = this._imageLookUp.bind(this);
        this.generatePdf = this._generatePdf.bind(this)
    }

    _generatePdf(req, res, next) {
        const user = req.user;
        const {users, fromdate,todate }= req.body
        console.log('users =====>', req.body)
        const filePath = "reports/users_report.pdf";
        ReportController._printUsersReport({user, users, fromdate,todate}, filePath).then((responseData) => {
            //console.log('FileManager._generatePdf.responseData ===>', responseData)
            const file = fs.createReadStream(filePath);
            const stat = fs.statSync(filePath);
            res.setHeader('Content-Length', stat.size);
            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'attachment; filename=users_report.pdf');
            res.download(filePath)
            file.pipe(res);
        }).catch(err => {
            console.log('FileManager._generatePdf.error ==>', err)
            res.send({success: false, message: err})
        })
        /*const rpt = new Report("reports/Repord.pdf")
            .pageHeader(["Users Summary"])
            .data(data)
            .detail([['name', 200], ['age', 50]])
            .render();

        */

    }

    _uploadImage(req, res) {
        const query = req.query
        const file = req.file
        const headers = req.headers
        const commandId = query.commandId
        const update = query.update
        const image = {
            data: fs.readFileSync(file.path),
            contentType: file.mimetype,
            addedBy: headers.userid
        };
        if (update) {
            this._updateImage(update, image, res)
        } else {
            this._createImage(image, commandId, res)
        }
    }

    _updateImage(imageId, newImage, res) {
        Image.findOne({_id: imageId}, (err, image) => {
            if (err) return res.send({success: false, message: err});
            image.data = newImage.data
            image.contentType = newImage.contentType

            image.save((err, savedImage) => {
                if (err) return res.send({success: false, message: err});
                return res.send({success: true, item: savedImage})
            })
        })
    }

    _createImage(image, commandId, res) {
        Image.create(image, (err, savedImage) => {
            if (err) return res.send({success: false, message: err});
            switch (commandId) {
                case 'add-face':
                    this._addFaceToUser(savedImage, res)
                    break;
                default:
                    return res.send({success: true, item: savedImage})
                    break;
            }
        })
    }

    _addFaceToUser(image, res) {
        User.findOne({_id: image.addedBy}, (err, user) => {
            if (err) res.send({success: false, message: err});
            user.faces = _.union(user.faces, [image._id])
            user.save((err, user) => {
                if (err) return err;
            })
            return res.send({success: true, item: image._id})
        })
    }

    _updateImage(req, res) {

    }

    _imageLookUp(req, res) {
        const params = req.params
        Image.findOne({_id: params.id}, (err, image) => {
            if (err) return res.send({success: false, messages: err})
            return res.send(image.data)
        })
    }
}

module.exports = new FileManager()