import _ from 'lodash'
import Device from '../models/model.device'
import User from '../models/model.user'
import GeoApi from '../api/GeoApi'
import Location from '../models/model.location'

class DeviceManager {
    constructor() {
        this.addDevice = this._addDevice.bind(this)
        this.ping = this._ping.bind(this)
        this.retrieveDevices = this._retrieveDevices.bind(this)
        this.updateLocation = this._updateLocation.bind(this)
    }

    _updateLocation(onSendMessage, uniqueId, coords) {
        GeoApi.reverseGeoCode(coords).then((address) => {
            Location.findOne({uniqueId: uniqueId}, (err, location) => {
                console.log('DeviceManager.updateLocation.address ====>', address)
                if (err) onSendMessage({
                    success: false,
                    message: err,
                    'command-id': 'update-location'
                })// throw err
                if (location) {
                    location.formattedAddress = address
                    location.coords = {
                        lat: coords.latitude,
                        lng: coords.longitude
                    }
                    location.updatedOn = Date.now()
                    location.save((err, loc) => {
                        if (err) onSendMessage({
                            success: false,
                            message: err,
                            'command-id': 'update-location'
                        })// throw err
                        onSendMessage({
                            success: true,
                            item: loc,
                            'command-id': 'update-location'
                        })
                    })
                } else {
                    const newLocation = {
                        uniqueId,
                        updatedOn: Date.now(),
                        coords: {
                            lat: coords.latitude,
                            lng: coords.longitude
                        },
                        formattedAddress: address
                    }
                    Location.create(newLocation, (err, loc) => {
                        if (err) onSendMessage({
                            success: false,
                            message: err,
                            'command-id': 'update-location'
                        })// throw err
                        onSendMessage({
                            success: true,
                            item: loc,
                            'command-id': 'update-location'
                        })
                    })
                }
            })
        }).catch((err) => {
            console.log('DeviceManager.updateLocation.err ===>', err)
            onSendMessage({
                success: false,
                message: err,
                'command-id': 'update-location'
            })
        })
    }

    _retrieveDevices(req, res) {
        const userId = req.user._id
        if (userId) {
            Device.find({'addedBy.userId': userId}, (err, devices) => {
                if (err) return res.send({success: false, message: err})
                return res.send({success: true, items: devices})
            })
        } else {
            return res.send({success: false, message: `Couldn't retrieve devices`})
        }
    }

    _addDevice(req, res) {
        return new Promise((reslove, reject) => {
            const {deviceInfo} = req.body
            Device.findOne({deviceId: deviceInfo.deviceId}, (err, device) => {
                if (err) reject(err)
                if (device) {
                    reject('Device already registered')
                } else {
                    const data = this._constructDeviceData(req)
                    this._createDevice(data, reslove, reject)
                }
            })
        })
    }

    _createDevice(data, resolve, reject) {
        Device.create(data, (err, device) => {
            if (err) reject(err)
            resolve(device)
        })
    }

    _constructDeviceData(req) {
        const headers = req.headers
        const {deviceInfo, simData} = req.body
        return {
            uniqueId: deviceInfo.uniqueId,
            deviceId: deviceInfo.deviceId,
            locale: deviceInfo.locale,
            country: deviceInfo.country,
            brand: deviceInfo.brand,
            manufacturer: deviceInfo.manufacturer,
            timezone: deviceInfo.timezone,
            userAgent: deviceInfo.userAgent,
            addedBy: {
                userId: headers.userid,
                owner: headers.username,
                'app-details': {
                    bundleId: deviceInfo.bundleId,
                    appVersion: deviceInfo.appVersion,
                    buildNumber: deviceInfo.buildNumber,
                }
            },
            simData: {
                phoneNumber: simData.phoneNumber,
                carrier: simData.carrier,
                countryCode: simData.countryCode,
                simInfo: simData.simInfo
            }
        }
    }

    _ping(req, res) {
        console.log('Server.route.ping ------------>', req.headers);
        return res.json({success: true, message: 'Ping successsful'})
    }
}
module.exports = new DeviceManager()