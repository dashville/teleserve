import User from '../../models/model.user';
import Device from '../../models/model.device';
import Location from '../../models/model.location';
import FileManager from '../FileManager'
import _ from 'lodash'

class Admin {
    constructor() {
        this.retrieveUsers = this._retrieveUsers.bind(this)
        this.retrieveDevices = this._retrieveDevices.bind(this)
        this.retrieveUserDetails = this._retrieveUserDetails.bind(this)
        this.modifyRoles = this._modifyRoles.bind(this);
        this.updateUserDetails = this._updateUserDetails.bind(this);
        this.deleteUsers = this._deleteUsers.bind(this);
        this.retrieveDeviceDetails = this._retrieveDeviceDetails.bind(this)
        this.generateUsersReport = this._generateUsersReport.bind(this)
    }
    _generateUsersReport(req,res){
        FileManager.generatePdf(req,res);
    }


    _retrieveUsers(req, res) {
        const fieldFilter = {_id: 1, username: 1, name: 1, lastname: 1, createdOn: 1, faces:1, admin:1, mainDevice:1}
        User.find({}, fieldFilter, (err, users) => {
            if (err) return res.send({success: false, message: err});
            return res.send({success: true, items: users})
        })
    }

    _deleteUsers(req, res) {
        const ids = req.body
        User.remove({_id: {$in: ids}}, (err, users) => {
            if (err) return res.send({success: false, message: err});
            console.log('Admin.deleteUsers.users ===> ', users)
            return res.send({success: true, items: ids})
        })
    }

    _retrieveUserDetails(req, res) {
        const userId = req.params.id
        const fieldFilter = {};//{_id: 1, username: 1,name: 1, lastname: 1, createdOn:1}
        User.findOne({_id: userId}, fieldFilter, (err, user) => {
            if (err) return res.send({success: false, message: err});
            return res.send({success: true, item: user})
        })
    }

    _updateUserDetails(req, res) {
        const userId = req.params.id
        const updates = req.body
        const updatedOn = new Date().getTime();

        User.findOneAndUpdate({_id: userId}, {$set: _.extend({}, updates, {updatedOn})}, {new: true}, (err, user) => {
            if (err) return res.send({success: false, message: err});
            console.log('Admin.user.update ===>', user)
            return res.send({success: true, item: user})
        })
    }

    _modifyRoles(req, res) {
        const userId = req.params.id
        const roles = req.body
        const admin = 'admin' === _.find(roles, r => r === 'admin')
        const updatedOn = new Date().getTime();
        User.findOneAndUpdate({_id: userId}, {$set: {roles, admin, updatedOn}}, {new: true}, (err, user) => {
            if (err) return res.send({success: false, message: err});
            return res.send({
                success: true,
                item: {
                    updatedOn: user.updatedOn,
                    roles: user.roles
                }
            })
        })
    }

    _retrieveDevices(req, res) {
        const fieldFilter = {_id: 1, uniqueId: 1, brand: 1, manufacturer: 1, createdOn: 1, addedBy: 1}
        Device.find({}, fieldFilter, (err, devices) => {
            if (err) return res.send({success: false, message: err});
            return res.send({success: true, items: devices})
        })
    }

    _retrieveDeviceDetails(req, res) {
        console.log('Admin._retrieveDeviceDetails.req.id ===> ',req.params.id )
        Device.findOne({_id: req.params.id}, (err, device) => {
            if (err) return res.send({success: false, message: err});
            if (device) {

                Location.findOne({uniqueId: device.uniqueId}, (err, loc) => {
                    if (err) return res.send({success: false, message: err});
                     console.log('Admin._retrieveDeviceDetails.loc ===> ',loc)
                    return res.send({success: true, item: device, location: loc})
                })
            }

        })
    }
}

module.exports = new Admin()