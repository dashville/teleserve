import {Report} from 'fluentreports';
import moment from 'moment';
import _ from 'lodash'

class ReportController {
    constructor() {
        this.printUsersReport = this._printUsersReport.bind(this)
    }

    _printUsersReport(params, filePath) {
        'use strict';
        return new Promise((resolve, reject) => {
            const adminUser = params.user;
            const mydata = params.users;
            const {fromdate, todate} = params

            const requestedBy = function (rpt, data) {
                rpt.print([
                    "Report requested by: ",
                    data.name + ' ' + data.lastname,
                    data.username,
                    data.phone,
                ], {x: 80});
            };

            const message = function (rpt) {

                const msg = [
                    'Please note that the following data is Sensitive and very Confidential',
                ];
                rpt.print(msg, {textColor: 'red'});
            };

            const header = function (rpt, data) {
                if (!data && data.id) {
                    return;
                }
                console.log('data ===>', data)

                // Date Printed - Top Right
                rpt.fontSize(9);
                rpt.print(new Date().toString('MM/dd/yyyy')); //, {y: 30, align: 'right'});

                // Report Title
                rpt.print('USERS SUMMARY REPORT', {fontBold: true, fontSize: 16, align: 'right'});

                rpt.print(`FROM   ${fromdate.valueOf().substr(0, fromdate.valueOf().indexOf('T'))}   TO   ${todate.valueOf().substr(0, todate.valueOf().indexOf('T'))}`, {fontSize: 10, align: 'right'});
                // Contact Info
                requestedBy(rpt, adminUser);

                rpt.newline();
                rpt.newline();
                rpt.newline();

                // Message
                message(rpt);

                rpt.newline();
                rpt.newline();
                rpt.newline();

                // Detail Header
                rpt.fontBold();
                rpt.band([
                    {data: 'Total Users', width: 80},
                    {data: 'Admin Users', width: 80},
                    {data: 'Users with Faces', width: 80},
                    {data: 'Users with Activated Devices', align: 3, width: 125}
                ]);
                rpt.fontNormal();
                rpt.bandLine();
            };

            let detailCount = 1
            const detail = function (rpt, data) {
                if (detailCount == 1) {
                    rpt.band([
                        {data: _.size(mydata), width: 80},
                        {data: _.size(_.filter(mydata, u => u.admin)), width: 80},
                        {data: _.size(_.filter(mydata, u => _.size(u && u.faces) > 0)), width: 80},
                        {data: _.size(_.filter(mydata, u => u && u.mainDevice)), width: 125}
                    ], {border: 1, width: 0});
                    detailCount++;
                }
            };
            let finalSummaryCount = 1
            const finalSummary = function (rpt, data) {
                rpt.newline();
                rpt.newline();
                rpt.fontBold();
                rpt.print('Last 5 signup:')
                rpt.standardFooter([''])
                rpt.newline();

                rpt.fontNormal();
                _.map(_.slice(_.reverse(mydata), 0, 5), u => {
                    rpt.print("Names: " + u.name + ' ' + u.lastname)
                    rpt.print("Email: " + u.username)
                    rpt.print("Registered: " + moment(Number(u.createdOn)).format('DD/MM/YYYY HH:mm'))
                    rpt.newline();
                })
                rpt.newline();
                rpt.newline();
                rpt.print('The end!', {align: 'right'});
            };

            // Optional -- If you don't pass a report name, it will default to "report.pdf"

            try {
                const rptName = "report.pdf";
                const resultReport = new Report(filePath || rptName)
                    .data(mydata)
                // You can Chain these directly after the above like I did or as I have shown below; use the resultReport constiable and continue chain the report commands off of it.  Your choice.
                // Settings
                resultReport
                    .fontsize(9)
                    .margins(40)
                    .detail(detail)
                    .groupBy('id')
                    .footer(finalSummary)
                    .header(header, {pageBreakBefore: true})
                ;
                resultReport.printStructure();

                //console.time("Rendered");
                resultReport.render();
                resolve(resultReport);
            } catch (e) {
                reject(e);
            }
        })
    }
}

export default new ReportController();