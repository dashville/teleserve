import passport from 'passport'
import EncryptUtils from "../utils/EncryptUtils"
import AuthUtils from "../utils/AuthUtils"
import User from '../models/model.user'
import UUID from 'uuid/v1';

class Authenticate {
    constructor() {
        this.login = this._onlogin.bind(this);
        this.logout = this._logout.bind(this);
        this.signup = this._signup.bind(this);
        this.resetPassword = this._resetPassword.bind(this);
        this.authenticate = this._authenticate.bind(this)
        this.vaidateToken = this._validateToken.bind(this)
    }

    _authenticate(req, res, next) {
        passport.authenticate('jwt', {session: false}, (err, user, info) => {
            if (err) {
                res.send({success: false, message: err})
            }
            if (user) {
                if (!user.admin && req.baseUrl.includes('/admin')) {
                    return res.status(401).json({
                        success: false, message: 'User Unauthorized',
                        status: 'error', code: 'Unauthorized'
                    })
                }
                req.user = user;
                return next()
            } else {
                return res.status(401).json({
                    success: false, message: 'User Unauthorized',
                    status: 'error', code: 'Unauthorized'
                })
            }
        })(req, res, next);
    }

    _onlogin(req, res) {
        const body = req.body
        //check token and automatically log in
        User.findOne({'username': body.username.toLowerCase().trim()}, (err, user) => {
            if (err) return this._handleError(err, res);

            if (user) {
                EncryptUtils.comparepwd(body.password, user.password).then((isPwdMatching) => {
                    if (isPwdMatching) {
                        this._onLoginSuccessful(user, res)
                    } else {
                        res.status(401).json({success: false, message: "Invalid username or password"})
                    }
                })
            } else {
                res.status(401).json({success: false, message: "User doesn't exist"})
            }
        })
    }

    _validateToken(req, res) {
        const {authorization} = req.headers;
        if (authorization) {
            AuthUtils.validateToken(authorization).then((decodedToken) => {
                if (decodedToken) {
                        User.findOne({_id: decodedToken.sub}, (err, user) => {
                            if (err) res.send({success: false, message: err});

                            if (user && decodedToken.pwd === user.password) {
                                return res.send({success: true, item: user, token: authorization});
                            } else {
                                res.send({success: false, message: "Invalid username or password."});
                            }
                        })
                } else {
                    res.send({success: false, message: "Token authentication failed."})
                }

            }).catch((err) => {
                res.send({success: false, message: 'Token needed'})
            })
        } else {
            res.send({success: false, message: "Token authentication failed."})
        }
    }

    _onLoginSuccessful(user, res) {

        /*
         iss:  spazaserve (issuer)
         sub: userid (subject)
         aud: spazawebapp (audience)
         exp: time (expirationtime)
         iat: (issued at)
         jti (jwt id)
         */

        const payload = {
            iss: 'teleserve',
            sub: user.id || user._id,
            pwd: user.password,
            admin: user.admin,
            iat: Date.now(),
            jti: UUID()
        }

        AuthUtils.generateToken(payload).then((token) => {
            return res.json({success: true, item: user, token})
        }).catch((err) => {
            res.send({success: false, message: err})
            console.log('Authenticate._onLoginSuccessful. err ->', err)
        })
    }

    _logout(req, res) {
        //destroy token
    }

    _signup(req, res) {
        const {body} = req
        User.findOne({username: body.email.toLowerCase().trim()}, (err, user) => {
            if (err) return this._handleError(err, res);
            if (user) {
                res.json({success: false, message: 'user already exist'})
            } else {
                this._createUser(body, res)
            }
        })
    }

    _resetPassword(req, res) {
        //TODO
    }

    _createUser(data, res) {
        EncryptUtils.encryptpwd(data.password.trim()).then((hashedPassword) => {
            return {
                username: data.email.toLowerCase().trim(),
                name: data.name.trim(),
                lastname: data.lastname.trim(),
                password: hashedPassword,
                phone: data.phone.trim(),
                email: data.email.toLowerCase().trim(),
            }
        }).then((newUser) => {
            User.create(newUser, (err, user) => {
                if (err) return this._handleError(err, res);
                this._onLoginSuccessful(user, res)
            })
        }).catch((err) => {
            console.log('Authenticate._createUser failed ----->', err)
        })
    }

    _handleError(err, res) {
        console.log('server.controllers.autheticate -------->', err)
        return res.status(401).send({
            success: false,
            message: err
        })
    }
}

module.exports = new Authenticate();