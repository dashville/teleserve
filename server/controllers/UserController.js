import _ from 'lodash'
import User from '../models/model.user'
import DeviceManager from '../controllers/DeviceManager'

class UserController {
    constructor() {
        this.retreiveFaces = this._retrieveFaces.bind(this)
        this.activateIdentity = this._activateIdentity.bind(this)
        this.updateProfile = this._updateProfile.bind(this)
        this.retrieveProfile = this._retrieveProfile.bind(this);
    }

     _retrieveProfile(req,res){
        if(req.user){
            res.send({success: true, item: req.user})
        }else{
            res.send({success: false, message: "Could't retrieve profile"})
        }
    }
    _activateIdentity(req, res) {
        const userId = req.headers.userid
        User.findOne({_id: userId}, (err, user) => {
            if (err) return res.send({success: false, message: err})
            const hasEnoughFaces =  (user && user.faces) ? _.size(user.faces) > 2 : false

            if (hasEnoughFaces) {

                DeviceManager.addDevice(req, res).then((device) => {
                    this._allocateDeviceToUser(user, device, res)
                }).catch((err) => {
                    console.log('UserController._activateIdentity.err ===>', err)
                    return res.send({success: false, message: err})
                })
            } else {
                return res.send({success: false, message: 'Not enough faces captured'})
            }
        })
    }

    _allocateDeviceToUser(user, device, res) {
        const deviceId = device._id
        const mainDevice = user.mainDevice
        if (!mainDevice) {
              user.mainDevice = deviceId
        }
        user.devices = _.union(user.devices, [deviceId])
        user.hasIdentity = true
        user.save((err, user) => {
            if (err) return res.send({success: false, message: err})
            return res.send({success: true, item: user})
        })
    }

    _retrieveFaces(req, res) {
        const params = req.params
        User.findOne({_id: params.id}, (err, user) => {
            if (err) return res.send({success: false, message: err})
            return res.send({success: true, items: user.faces})
        })
    }
    _updateProfile(req,res){
        console.log('UserController.req.res ====>', req)
        const user = req.user
        const updates = req.body
        const updatedOn = Date.now();
        User.findOneAndUpdate({_id: user.id || user._id}, {$set: _.extend({}, updates, {updatedOn})}, {new: true}, (err, user) => {
            if (err) return res.send({success: false, message: err});
            return res.send({success: true, item: user})
        })
    }
}
module.exports = new UserController()