import React, {Component} from 'react'
import _ from 'lodash'


export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                username: '',
                password: ''
            },
            authFailed: false
        }
    }

    _onLogin() {
        const {app} = this.props
        const {form} = this.state
        const path = `/mobile/v1/auth/login`;
        const headers = app.requestHeaders();
        //app.showActicity()
        fetch(path, {
            method: 'POST',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-type': 'application/json'
            }), body: JSON.stringify(form)
        }).then(app.responseToJson(path)).then((respData) => {
            //app.hideActicity()
            if (respData.success) {
                app.onUserChanged(respData.item)
                _.defer(()=>{
                    this.props.history.replace('/')
                })
                //AuthUtils.loginSuccessful(app, navigation, respData.item, respData.token)
            } else {
               this.setState({authFailed: true, authFailedMsg: respData.message})
            }
        }).catch((err) => {
           // app.hideActicity()
            console.log('Login._onLogin err ---->', err)
        })
    }

    render() {
        const {form} = this.state
        return (
            <div className="card card-login mx-auto mt-5">
                <div className="card-header">Login</div>
                <div className="card-body">
                    <form>
                        {this.state.authFailed ?
                            <label className="alert-danger">
                                {this.state.authFailedMsg}
                            </label> : null
                        }
                        <div className="form-group">
                            <label htmlFor="inputUsername">Username</label>
                            <input className="form-control" id="exampleInputEmail1" type="email"
                                   aria-describedby="emailHelp" placeholder="Enter username"
                             onChange={(e) => {
                                 this.setState({form: _.extend({}, form, {username: e.target.value})})
                             }
                             }/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="inputPassword1">Password</label>
                            <input className="form-control" id="exampleInputPassword1" type="password"
                                   placeholder="Password" onChange={(e)=>{
                                       this.setState({form: _.extend({}, form, {password: e.target.value})})
                                   }
                            }/>
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <label className="form-check-label">
                                    <input className="form-check-input" type="checkbox"/> Remember Password</label>
                            </div>
                        </div>

                            <a className="btn btn-primary btn-block" onClick={this._onLogin.bind(this)}>Login</a>
                    </form>
                </div>
            </div>
        )
    }
}