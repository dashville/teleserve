import React, {Component} from 'react';
import {Route} from 'react-router-dom'
import _ from 'lodash';
import './styles/App.css';
import {MasterContent, NavigationBar} from './components'
import Login from './Login'
import Home from './Home'
import UserDetails from './pages/admin/UserDetails'
import  {PAGES} from './pages/pages'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            roles: ['admin', 'user', 'capture']
        };
        this.systemId = 'teleport';
        this.appId = 'teleportal';
        this.responseToJson = this._responseToJson.bind(this);
        this.requestHeaders = this._requestHeaders.bind(this);
        this.onUserChanged = this._onUserChanged.bind(this);
        this.setCurrentPath = this._setCurrentPath.bind(this);
    }

    _requestHeaders() {
        const user = this.state.user
        return _.extend({}, user ? {
            userId: user.id,
            username: user.username,
            token: user.token,
            provider: user.provider,
            systemId: this.systemId,
            appId: this.appId
        } : {})
    }

    _onUserChanged(user) {
        this.setState({user})
    }

    _responseToJson(url) {
        return (response) => {
            try {
                return response.json()
            } catch (err) {
                console.log(`App.responseToJson failed. ----${url}--->`, err)
            }
        }
    }

    _setCurrentPath(path) {
        this.setState({currentPath: path})
    }

    _addPropsToRoute(WrappedComponent, passedProps) {

        return (
            class Route extends Component {
                constructor(props) {
                    super(props)
                    this._setCurrentPath()
                }

                _setCurrentPath() {
                    const app = passedProps.app
                    const {pathname} = this.props.location
                    if (app.state.currentPath !== pathname) {
                        app.setCurrentPath(pathname)
                    }
                }

                render() {
                    let props = Object.assign({}, this.props, passedProps)
                    return <WrappedComponent {...props} />
                }
            }
        )
    }

    render() {
        const props = _.extend({}, {app: this})
        return (
            <div className="App">
                <NavigationBar   {...props }/>
                <Route exact path="/" render={routeProps=> <Home{...routeProps}{...props} />}/>
                <Route path="/login" render={routeProps=> <Login{...routeProps}{...props} />}/>
                <Route path="/admin" render={routeProps=> <MasterContent{...routeProps}{...props} />}/>
            </div>
        );
    }
}

export default App;
