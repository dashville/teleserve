import React, {Component} from 'react'

export default class BreadcrumHeader extends Component {
    render() {
        const {title} = this.props
        return (
            <ol className="breadcrumb">
                <li className="breadcrumb-item">
                    <a onClick={()=> {}}>Dashboard</a>
                </li>
                <li className="breadcrumb-item active">{title}</li>
            </ol>
        )
    }
}