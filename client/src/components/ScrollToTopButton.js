import React, {Component} from 'react'

export default class ScrollToTopButton extends Component {
    render() {
        return (
            <a className="scroll-to-top rounded" href="#page-top">
                <i className="fa fa-angle-up"></i>
            </a>
        )
    }
}