import React, {Component} from 'react'
import {Link} from 'react-router-dom'

export default class SidePanel extends Component {
    render() {
        return (
            <ul className="navbar-nav navbar-sidenav" id="exampleAccordion"
            >
                <li className="nav-item" data-toggle="tooltip" data-placement="right"
                    title="Dashboard" >
                    <Link to="/admin/" className="nav-link" >
                        <i className="fa fa-fw fa-dashboard"/>
                        <span className="nav-link-text">Dashboard</span>
                    </Link>
                </li>
                <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Devices">
                    <Link to="/admin/devices" className="nav-link">
                        <i className="fa fa-fw fa-mobile"/>
                        <span className="nav-link-text">Devices</span>
                    </Link>
                </li>
                <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Users">
                    <Link to="/admin/users" className="nav-link">
                        <i className="fa fa-fw fa-users"/>
                        <span className="nav-link-text">Users</span>
                    </Link>
                </li>
            </ul>
        )
    }
}