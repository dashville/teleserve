import React, { Component } from 'react'

export default class SearchBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchText: ''
        }
    }

    _onChangeText(e) {
        const text = e.target.value
        this.setState({ searchText: text })
        this.props.onChangeText(text)
    }

    render() {
        const {placeholder, type} = this.props
        return (
            <form className="form-inline my-2 my-lg-0 mr-lg-2">
                <div className="input-group">
                    <input className="form-control" type={type} placeholder={placeholder}
                           onChange={this._onChangeText.bind(this)} value={this.state.searchText}
                    />
                </div>
            </form>

        )
    }
}