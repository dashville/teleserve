import React, {Component} from 'react'
import {Footer, BreadcrumHeader, ScrollToTopButton, LogoutModal} from './components/index'
import {Dashboard, Users, Devices, UserDetails,DeviceDetails} from './pages/index'
import {Route} from 'react-router-dom'

export default class MasterContent extends Component {

    render() {
        const props = this.props
        return (
            <div className="content-wrapper">
                <div className="container-fluid">
                    <BreadcrumHeader/>

                    <Route
                        exact path="/admin"
                        render={routeProps => <Dashboard  {...props}{...routeProps}/>}
                    />
                    <Route
                        path="/admin/devices"
                        render={routeProps => <Devices  {...props}{...routeProps}/>}
                    />
                    <Route
                        path="/admin/device/:id"
                        render={routeProps => <DeviceDetails  {...props}{...routeProps}/>}
                    />
                    <Route
                        path="/admin/users"
                        render={routeProps => <Users  {...props}{...routeProps}/>}
                    />
                    <Route
                        path="/admin/user/:id"
                        render={routeProps => <UserDetails {...props} {...routeProps}/>}
                    />
                </div>
                <Footer/>
                <ScrollToTopButton />
                <LogoutModal />
            </div>
        )
    }
}