import Dashboard from './admin/Dashboard'
import Users from './admin/Users'
import Devices from './admin/Devices'
import UserDetails from './admin/UserDetails'
import DeviceDetails from './admin/DeviceDetails'
export {
    Dashboard,
    Users,
    Devices,
    UserDetails,
    DeviceDetails
}