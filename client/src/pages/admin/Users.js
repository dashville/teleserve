import React, {Component} from 'react';
import {SearchBox} from '../../components'
import {Link} from 'react-router-dom'
import _ from 'lodash';
import moment from 'moment';

class UsersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: props.items
        }

    }

    componentWillReceiveProps(nextProps) {
        if (_.differenceWith(nextProps.items, this.props.items, _.isEqual)) {
            this.setState({users: nextProps.items})
        }
    }

    _onChange(e, u) {
        const isChecked = e.target.checked
        const users = _.map(this.state.users, (user) => {
            if (user._id === u._id) {
                user.isChecked = isChecked
            }
            return user
        })
        this.setState({users})
    }

    _renderItem(user, index) {
        return (
            <tr key={`_user_${index}`}>
                <td>
                    <input type="checkbox" name="select" checked={user.isChecked }
                           onChange={(e) => this._onChange(e, user)}
                    />
                </td>
                <td>
                    <Link to={`/admin/user/${user._id}`}>
                        {user.username}</Link>
                </td>
                <td> {user.name + ' ' + user.lastname}</td>
                <td>{moment(Number(user.createdOn)).format('DD MMM YY, HH:mm')}</td>
            </tr>
        )
    }

    render() {
        const {users} = this.state
        return (
            <tbody>
            {
                _.map(users, (user, index) => {
                    return this._renderItem(user, index)
                })
            }
            </tbody>
        )
    }
}
export default class Users extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [],
            searchResultsCount: 0, showSearchCount: false
        }
    }

    componentDidMount() {
        this._fetchUsers();
        this._Mounted = true;
    }

    componentWillUnmount() {
        this._Mounted = false;
    }

    _fetchUsers() {
        const {app} = this.props
        const headers = app.requestHeaders()
        const path = '/admin/users'
        fetch(path, {
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(path)).then((responseData) => {
            if (responseData.success && this._Mounted) {
                this.users = _.map(responseData.items, (user) => {
                    user.isChecked = false
                    return user
                })
                this.setState({users: responseData.items})
            }
        }).catch((err) => {
            console.log('Users.componentDidmount -------->', err)
        })
    }

    _onSelectAll(e) {
        const isChecked = e.target.checked
        const users = _.map(this.state.users, (user) => {
            user.isChecked = isChecked
            return user
        })
        if (this._Mounted) {
            this.setState({users, selectAllIsChecked: isChecked})
        }
    }

    _renderHeader() {
        return (
            <thead>
            <tr>
                <th>
                    <input type="checkbox" name="selectAll" value={this.state.selectAll}

                           onChange={this._onSelectAll.bind(this)}/>
                </th>
                <th>Username</th>
                <th>Names</th>
                <th>Registered On</th>
            </tr>
            </thead>
        )

    }

    _deleteUsers() {
          let ids = [];
        _.filter(this.state.users, user => {
            if (user.isChecked) {
                ids = _.union(ids, [user._id]);
            }
            return user.isChecked
        });
       if (ids.length > 0) {
            const confirmed = window.confirm('You are about to delete the selected users, Click Ok to continue')

            if(confirmed) {
                const app = this.props.app;
                const headers = app.requestHeaders();
                const path = '/admin/users/delete'
                fetch(path, {
                    method: "POST",
                    headers: _.extend({}, headers, {
                        'Accept': 'application/json', 'Content-Type': 'application/json'
                    }),
                    body: JSON.stringify(ids)
                }).then(app.responseToJson(path)).then((responseData) => {
                    console.log('Users._deleteUsers.responseData ===> ', responseData)
                    if (responseData.success && this._Mounted) {
                        const ids = responseData.items;
                        const items = this.state.users;
                        const users = _.filter(items, item => {
                            return !_.find(ids, id => id === item._id)
                        });

                        this.users = users;
                        this.setState({users})
                    }
                }).catch((err) => {
                    console.log('Users._deleteUsers -------->', err)
                })
            }
        }
    }

    _renderFooter() {
        return (
            <tfoot>
            <tr>
                <th>
                    <button className="btn btn-danger" onClick={this._deleteUsers.bind(this)}>Delete</button>
                </th>
                <th>Username</th>
                <th>Names</th>
                <th>Registered On</th>
            </tr>
            </tfoot>
        )
    }

    _onChangeSearchText(text) {
        if (text) {
            const filterUsers = _.filter(this.users, (user) => {
                return (user.name && user.name.includes(text)) || (user.lastname && user.lastname.includes(text)) || (user.username && user.username.includes(text))
            })
            if (this._Mounted) {
                this.setState({users: filterUsers, searchResultsCount: _.size(filterUsers), showSearchCount: true})
            }
        } else {
            if (this._Mounted) {
                this.setState({users: this.users, showSearchCount: false})
            }
        }
    }

    render() {
        const props = this.props
        const app = props.app
        const {users, showSearchCount, searchResultsCount} = this.state
        const hasUsers = _.size(users) > 0
        return (
            <div className="card mb-3">
                <div className="card-header" style={{flexDirection: 'row'}}>
                    <i className="fa fa-table"/>
                    <SearchBox {...props}
                               type="text" placeholder={"Search for user"}
                               onChangeText={this._onChangeSearchText.bind(this)}
                    />
                    {showSearchCount ?
                        <span>{`Showing: ${searchResultsCount}`}</span> : null}
                </div>
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            {this._renderHeader()}
                            {this._renderFooter()}

                            {hasUsers ? <UsersList app={app} items={users}/> : null}

                        </table>
                    </div>
                </div>

                <div className="card-footer small text-muted">
                    Updated on {moment(new Date().getTime()).format('DD MMM YY, HH:mm')}
                </div>
            </div>
        )
    }
}