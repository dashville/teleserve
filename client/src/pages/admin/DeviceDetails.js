import React, {Component} from 'react'
import _ from 'lodash';
let count = 1;
class DeviceDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: undefined,
        }
        this.mapsUrl = "http://www.google.com/maps/place/";
    }

    componentDidMount() {
        this._fetchDeviceDetails();
        this._Mounted = true;
    }

    componentWillUnmount() {
        this._Mounted = false;

    }

    _fetchDeviceDetails() {
        const {app, match} = this.props
        const headers = app.requestHeaders();
        const deviceId = match.params.id
        const path = `/admin/device/${deviceId}`

        fetch(path, {
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(path)).then((responseData) => {
            console.log(`DeviceDetails._fetchDevices.responseData ${count++}===>`, responseData)
            if (responseData.success) {
                this._Mounted &&
                this.setState({
                    device: responseData.item,
                    location: _.extend({}, responseData.location)
                })
            } else {
                alert(responseData.message)
            }
        }).catch((e) => {
            console.log('DeviceDetails._fetchDeviceDetails.err ===>', e)
        })
    }

    _renderLocation() {
        const {location} = this.state
        if (location) {
            const {coords} = location;
            return (
                <div>
                    <h4>location</h4>
                    <p></p>
                    {location ?
                        <a
                            target="_blank"
                            href={`${this.mapsUrl}${coords.lat},${coords.lng}`}>{coords.lat + ',' + coords.lng}</a>
                        : null}
                </div>
            )
        }
    }

    render() {
        return (
            <div>

                {this._renderLocation()}
            </div>
        )
    }
}
export default  DeviceDetails;