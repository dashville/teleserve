import React, {Component} from 'react';
import _ from 'lodash';
import moment from  'moment';
import {Link} from 'react-router-dom'

class DevicesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            devices: props.items
        }
    }

    _onSelect(){

    }

    _renderItem(device, index) {
        return (
            <tr key={`_device_${index}`}>
                <td>  <Link to={`/admin/device/${device._id}`}>
                    {device.brand + '-' + device.uniqueId.substr(-6)}</Link>
            </td>
                <td> {device.manufacturer}</td>
                <td>{moment(Number(device.createdOn)).format('DD MMM YY, HH:mm')}</td>
                <td> {device.owner}</td>
            </tr>

        )
    }

    render() {
        const {devices} = this.state
        return (
            <tbody>
            {
                _.map(devices, (device, index) => {
                    return this._renderItem(device, index)
                })
            }
            </tbody>
        )
    }
}
export default class Devices extends Component {
    constructor(props) {
        super(props)
        this.state = {
            devices: []
        }
    }

    componentDidMount() {
        this._fetchDevices();
        this._Mounted = true;
    }
    componentWillUnMount(){
         this._Mounted = false;
    }


    _fetchDevices() {
        const {app} = this.props
        const headers = app.requestHeaders()
        const path = '/admin/devices'
        fetch(path, {
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(path)).then((responseData) => {
            if (responseData.success && this._Mounted) {
                this.setState({devices: responseData.items})
            }
        }).catch((err) => {
            console.log('Devices.componentDidmount -------->', err)
        })
    }

    _renderHeader() {
        return (
            <thead>
            <tr>
               <th>Brand</th>
                <th>Manufacturer</th>
                <th>Added On</th>
                <th>Owner</th>
            </tr>
            </thead>
        )

    }

    _renderFooter() {
        return (
            <tfoot>
            <tr>
                <th>Brand</th>
                <th>Manufacturer</th>
                <th>Added On</th>
                <th>Owner</th>
            </tr>
            </tfoot>
        )
    }

    render() {
        const app = this.props.app
        const hasDevices = _.size(this.state.devices) > 0
        return (
            <div className="card mb-3">
                <div className="card-header">
                    <i className="fa fa-table"></i> Data Table Example
                </div>
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            {this._renderHeader()}
                            {this._renderFooter()}

                            {hasDevices ? <DevicesList app={app} items={this.state.devices}/> :null}

                        </table>
                    </div>
                </div>
               <div className="card-footer small text-muted">
                    Updated on {moment(new Date().getTime()).format('DD MMM YY, HH:mm')}
                </div>
            </div>
        )
    }
}