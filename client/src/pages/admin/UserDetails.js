import React, {Component} from 'react';
import moment from 'moment';
import _ from 'lodash';

const tabs = {
    DETAILS: 'details',
    FACES: 'face',
    ROLES: 'roles'
}

export default class UserDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: undefined,
            currentTab: tabs.DETAILS,
        }
    }

    componentWillMount() {
        this._fetchUser()
        this._Mounted = true;
    }

    componentWillUnmount() {
        this._Mounted = false;
    }

    _fetchUser() {
        const {app, match} = this.props
        const headers = app.requestHeaders()
        const userId = match.params.id
        const path = `/admin/user/${userId}`
        fetch(path, {
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(path)).then((responseData) => {
            if (responseData.success) {
                const user = responseData.item
                this._Mounted && this.setState({user})
            } else {
                alert(responseData.message)
            }
        }).catch((e) => {
            console.log('UserDetails._fetchUser.err ======>', e)
        })
    }

    _setValues(user) {
        _.defer(()=> {
            const {username, names, phone, email, phonealt} = this.refs;
            username.value = user.username;
            names.value = user.name + ' ' + user.lastname;
            phone.value = user.phone;
            email.value = user.email;
            phonealt.value = user['phone-alt'];
        })
    }

    _onHandleTabPress(tab) {
        this.setState({currentTab: tab})
    }

    _handleUpdate(e) {
        e.preventDefault();
        const {app} = this.props
        const headers = app.requestHeaders()
        const user = this.state.user;
        const userId = user.id || user._id
        const path = `/admin/user/${userId}/update`
        const updates = {
            email: this.refs.email.value,
            'phone-alt': this.refs.phonealt.value,
        }
        fetch(path, {
            method: "POST",
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            }),
            body: JSON.stringify(updates)
        }).then(app.responseToJson(path)).then((responseData) => {
            console.log('UserDetails._handleUpdate.responseData ===>', responseData)
            if (responseData.success) {
                const item = responseData.item
                this._Mounted && this.setState({user: _.extend({}, user, item)})
            } else {
                alert(responseData.message)
            }
        }).catch((e) => {
            console.log('UserDetails._handleUpdate.err ======>', e)
        })
    }

    _handleModifyRoles(roles) {
        const {app} = this.props
        const headers = app.requestHeaders()
        const user = this.state.user;
        const userId = user.id || user._id
        const path = `/admin/user/${userId}/modify-roles`
        fetch(path, {
            method: "POST",
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            }),
            body: JSON.stringify(roles)
        }).then(app.responseToJson(path)).then((responseData) => {
            if (responseData.success) {
                const item = responseData.item
                this._Mounted && this.setState({
                    user: _.extend({}, user, {
                        roles: item.roles,
                        updatedOn: item.updatedOn
                    })
                })
            } else {
                alert(responseData.message)
            }
        }).catch((e) => {
            console.log('UserDetails._handleModifyRoles.err ======>', e)
        })
    }

    _renderTabs() {
        return (
            <div className="card-header" style={{flexDirection: 'row'}}>
                <ul className="nav nav-tabs">
                    <li style={styles.tab}
                        className="active"
                        onClick={() => this._onHandleTabPress(tabs.DETAILS)}>Details
                    </li>
                    <li style={styles.tab}
                        onClick={() => this._onHandleTabPress(tabs.ROLES)}>Roles
                    </li>
                    <li style={styles.tab}
                        onClick={() => this._onHandleTabPress(tabs.FACES)}>Faces
                    </li>
                </ul>
            </div>
        )
    }


    _renderDetails() {
        const {user} = this.state;
        this._setValues(user)
        return (
            <form
                onSubmit={this._handleUpdate.bind(this)} style={{padding: 10}}
                className="container-fluid col-md-6"
            >
                <div className="form-group" style={styles.row}>
                    <label
                        className="col-sm-4 col-form-label"
                        style={styles.label}
                    >Username</label>
                    <input
                        type="text"
                        className="form-control"
                        ref="username"
                        disabled={true}
                    />
                </div>
                <div className="form-group" style={styles.row}>
                    <label className="col-sm-4 col-form-label"
                           style={styles.label}
                    >Names</label>
                    <input
                        type="text"
                        className="form-control"
                        ref="names"
                        disabled={true}
                    />
                </div>
                <div className="form-group" style={styles.row}>
                    <label className="col-sm-4 col-form-label"
                           style={styles.label}>Phone</label>
                    <input
                        type="text"
                        className="form-control"
                        ref="phone"
                        disabled={true}
                    />
                </div>

                <div className="form-group" style={styles.row}>
                    <label htmlFor="email" className="col-sm-4 col-form-label"
                           style={styles.label}>Email</label>
                    <input type="email" ref="email"
                           className="form-control"
                    />
                </div>
                <div className="form-group" style={styles.row}>
                    <label htmlFor="email" className="col-sm-4 col-form-label"
                           style={styles.label}>Alt. phone</label>
                    <input type="text" ref="phonealt"
                           className="form-control"
                    />
                </div>
                <input type="submit" value="Update Details"
                       className="btn btn-primary"
                />
            </form>
        )
    }

    _renderFaces() {
        const {user} = this.state
        return (
            <div className="card-body">
                {_.map(user.faces, faceId => {
                    return <img key={`face_${faceId}`}
                                src={`http://localhost:3000/images/${faceId}`}
                                style={{
                                    width: '200px',
                                    height: '200px',
                                    margin: 4,
                                }}
                    />
                })
                }
            </div>
        )
    }

    _renderRoles() {
        const {user} = this.state;
        const app = this.props.app;
        return (
            <div className="card-body">
                <form
                    onSubmit={(e) => {
                        e.preventDefault();
                        const role = this.refs.roles.value
                        if (role !== this.refs.chooseOption.value) {
                            const roles = _.union(user.roles, [role])
                            this._handleModifyRoles(roles)
                        }
                    }}

                    className="container-fluid col-md-6"
                    style={styles.row}>
                    <select className="custom-select mr-sm-2" ref="roles">
                        <option selected ref="chooseOption">Choose role...</option>
                        {_.map(app.state.roles, role => {
                            return <option value={role}>{role}</option>
                        })
                        }
                    </select>
                    <input type="submit" className="btn btn-primary" value="Add Roles"/>
                </form>
                <div className="container-fluid col-md-6">
                    <h4>Roles</h4>
                    {
                        _.map(user.roles, role => {
                            return (
                                <div style={styles.roles}>
                                    <p style={{flex: 1, textAlign: 'left'}}>{role}</p>
                                    <button
                                        className="btn btn-danger"
                                        disabled={role === 'user'}
                                        onClick={(e) => {
                                            e.preventDefault();
                                            const roles = _.filter(user.roles, r => r !== role)
                                            this._handleModifyRoles(roles)
                                        }}
                                    >
                                        <i className="fas fa-trash-alt"
                                           style={{color: 'white'}}/>
                                        remove
                                    </button>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

    render() {
        const user = this.state.user

        if (user) {
            const tstamp = Number(user.updatedOn || user.createdOn)
            return (
                <div className="card mb-3">
                    {this._renderTabs()}

                    <div className="tab-content ">
                        {(() => {
                            switch (this.state.currentTab) {
                                case tabs.DETAILS:
                                    return this._renderDetails();
                                case tabs.ROLES:
                                    return this._renderRoles();
                                case tabs.FACES:
                                    return this._renderFaces();
                                default:
                                    return null;
                            }
                        })()}
                    </div>
                    <div className="card-footer small text-muted">
                        Updated on {moment(tstamp).format('DD MMM YY, HH:mm')}
                    </div>
                </div>
            )
        }
        return null
    }
}
const styles = {
    tab: {
        'padding': '5px 15px'
    },
    active: {
        'color': 'white',
        'background-color': '#428bca',
    },
    row: {flexDirection: 'row', display: 'flex'},
    label: {textAlign: 'left'},
    roles: {
        flexDirection: 'row', display: 'flex',
        padding: 1, margin: 1,
        border: '0px 0px 1px 0px solid red'
    }
}