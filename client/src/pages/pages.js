export const PAGES = {
    DASHBOARD: 'dashboard',
    DEVICES: 'devices',
    USERS: 'users',
    USERDETAILS: 'user-details',
}