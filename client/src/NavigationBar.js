import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { SidePanel } from './components'

export default class Navbar extends Component {
    _renderOptions() {
        const props = this.props
        const { currentPath } = props.app.state
        const isLoginPath = currentPath === '/login'
        if (!isLoginPath) {
            return (<div className="collapse navbar-collapse" id="navbarResponsive">

                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to="/login">
                            <span style={{ color: 'white' }}>
                                <i className="fa fa-fw fa-sign-in" />
                                Login</span>
                        </Link>
                    </li>
                </ul>
            </div>
            )
        }
        return null
    }
    _onChangeSearchText(text) {

    }

    _renderUserOtions() {
        const props = this.props
        const user = props.app.state.user
        const { currentPath } = props.app.state
        const shouldShowSidePanel = currentPath !== '/' && currentPath !== '/login'
        console.log('Navbar._renderUserOtions =======>', shouldShowSidePanel)
        const isLoginPath = currentPath === '/login'
        const isHomePath = currentPath === '/'
        const isAdmin = true //user && user.isAdmin
        return (
            <div className="collapse navbar-collapse" id="navbarResponsive">
                {shouldShowSidePanel ? <SidePanel {...props} /> : null}
                  <ul className="navbar-nav sidenav-toggler">
                    <li className="nav-item">
                        <a className="nav-link text-center" id="sidenavToggler">
                            <i className="fa fa-fw fa-angle-left" />
                        </a>
                    </li>
                </ul>

                {isLoginPath ? null :
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown"
                                data-toggle="dropdown" style={{marginRight: 3}}
                                aria-haspopup="true" aria-expanded="true">
                                  <i className="fa fa-fw fa-user" />
                                 {user.name + ' ' + user.lastname}
                            </a>
                            <div className="dropdown-menu" aria-labelledby="messagesDropdown">

                                    {isAdmin && isHomePath ?
                                        <Link to="/admin" className="dropdown-item">
                                            <button className="btn btn-primary" type="button"
                                                onClick={() => { }}>Admin
                                            </button>
                                        </Link>
                                        : null
                                    }

                                <div className="dropdown-divider" />
                                <a className="dropdown-item"  data-toggle="modal" data-target="#exampleModal">
                                            <i className="fa fa-fw fa-sign-out" />Logout</a>
                                <div className="dropdown-divider" />
                            </div>
                        </li>
                    </ul>
                }
            </div>
        )
    }

    render() {
        const props = this.props
        const user = props.app.state.user
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav"
                style={{ alignItems: 'flex-start' }}>
                <Link className="navbar-brand" to="/">
                    Tele Portal
                </Link>

                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                {user ? this._renderUserOtions() : this._renderOptions()}
            </nav>
        )
    }
}